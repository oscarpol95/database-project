﻿namespace ProyectoBases
{
    partial class ingresoInfTer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ingresoInfTer));
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ubicacion = new System.Windows.Forms.Label();
            this.noArete = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.Label();
            this.raza = new System.Windows.Forms.Label();
            this.rpPadre = new System.Windows.Forms.Label();
            this.rgdPadre = new System.Windows.Forms.Label();
            this.rpMadre = new System.Windows.Forms.Label();
            this.propietario = new System.Windows.Forms.Label();
            this.rdg = new System.Windows.Forms.Label();
            this.Sexo = new System.Windows.Forms.Label();
            this.nombre = new System.Windows.Forms.Label();
            this.rp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "MASCULINO",
            "FEMENINO"});
            this.comboBox4.Location = new System.Drawing.Point(158, 125);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(129, 21);
            this.comboBox4.TabIndex = 80;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(430, 125);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(129, 21);
            this.comboBox3.TabIndex = 79;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(430, 35);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(129, 21);
            this.comboBox2.TabIndex = 78;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(156, 209);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(129, 21);
            this.comboBox1.TabIndex = 77;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(432, 157);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(131, 20);
            this.textBox12.TabIndex = 76;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(430, 80);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(131, 20);
            this.textBox11.TabIndex = 75;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(430, 197);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(131, 20);
            this.textBox10.TabIndex = 74;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(432, 246);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(131, 20);
            this.textBox7.TabIndex = 73;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(156, 250);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(131, 20);
            this.textBox6.TabIndex = 72;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(158, 164);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(131, 20);
            this.textBox4.TabIndex = 71;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(156, 80);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(131, 20);
            this.textBox2.TabIndex = 70;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(158, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 20);
            this.textBox1.TabIndex = 69;
            // 
            // ubicacion
            // 
            this.ubicacion.AutoSize = true;
            this.ubicacion.Location = new System.Drawing.Point(357, 42);
            this.ubicacion.Name = "ubicacion";
            this.ubicacion.Size = new System.Drawing.Size(55, 13);
            this.ubicacion.TabIndex = 68;
            this.ubicacion.Text = "Ubicación";
            // 
            // noArete
            // 
            this.noArete.AutoSize = true;
            this.noArete.Location = new System.Drawing.Point(357, 80);
            this.noArete.Name = "noArete";
            this.noArete.Size = new System.Drawing.Size(52, 13);
            this.noArete.TabIndex = 67;
            this.noArete.Text = "No. Arete";
            // 
            // estado
            // 
            this.estado.AutoSize = true;
            this.estado.Location = new System.Drawing.Point(357, 125);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(43, 13);
            this.estado.TabIndex = 66;
            this.estado.Text = "Estado:";
            // 
            // raza
            // 
            this.raza.AutoSize = true;
            this.raza.Location = new System.Drawing.Point(357, 160);
            this.raza.Name = "raza";
            this.raza.Size = new System.Drawing.Size(35, 13);
            this.raza.TabIndex = 65;
            this.raza.Text = "Raza:";
            // 
            // rpPadre
            // 
            this.rpPadre.AutoSize = true;
            this.rpPadre.Location = new System.Drawing.Point(357, 200);
            this.rpPadre.Name = "rpPadre";
            this.rpPadre.Size = new System.Drawing.Size(56, 13);
            this.rpPadre.TabIndex = 64;
            this.rpPadre.Text = "RP Padre:";
            // 
            // rgdPadre
            // 
            this.rgdPadre.AutoSize = true;
            this.rgdPadre.Location = new System.Drawing.Point(357, 250);
            this.rgdPadre.Name = "rgdPadre";
            this.rgdPadre.Size = new System.Drawing.Size(65, 13);
            this.rgdPadre.TabIndex = 63;
            this.rgdPadre.Text = "RGD Padre:";
            // 
            // rpMadre
            // 
            this.rpMadre.AutoSize = true;
            this.rpMadre.Location = new System.Drawing.Point(40, 257);
            this.rpMadre.Name = "rpMadre";
            this.rpMadre.Size = new System.Drawing.Size(58, 13);
            this.rpMadre.TabIndex = 62;
            this.rpMadre.Text = "RP Madre:";
            // 
            // propietario
            // 
            this.propietario.AutoSize = true;
            this.propietario.Location = new System.Drawing.Point(41, 209);
            this.propietario.Name = "propietario";
            this.propietario.Size = new System.Drawing.Size(57, 13);
            this.propietario.TabIndex = 61;
            this.propietario.Text = "Propietario";
            // 
            // rdg
            // 
            this.rdg.AutoSize = true;
            this.rdg.Location = new System.Drawing.Point(41, 164);
            this.rdg.Name = "rdg";
            this.rdg.Size = new System.Drawing.Size(34, 13);
            this.rdg.TabIndex = 60;
            this.rdg.Text = "RDG:";
            // 
            // Sexo
            // 
            this.Sexo.AutoSize = true;
            this.Sexo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Sexo.Location = new System.Drawing.Point(41, 125);
            this.Sexo.Name = "Sexo";
            this.Sexo.Size = new System.Drawing.Size(34, 13);
            this.Sexo.TabIndex = 59;
            this.Sexo.Text = "Sexo:";
            // 
            // nombre
            // 
            this.nombre.AutoSize = true;
            this.nombre.Location = new System.Drawing.Point(40, 80);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(47, 13);
            this.nombre.TabIndex = 58;
            this.nombre.Text = "Nombre:";
            // 
            // rp
            // 
            this.rp.AutoSize = true;
            this.rp.Location = new System.Drawing.Point(41, 35);
            this.rp.Name = "rp";
            this.rp.Size = new System.Drawing.Size(115, 13);
            this.rp.TabIndex = 57;
            this.rp.Text = "RP:                              ";
            // 
            // ingresoInfTer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 322);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ubicacion);
            this.Controls.Add(this.noArete);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.raza);
            this.Controls.Add(this.rpPadre);
            this.Controls.Add(this.rgdPadre);
            this.Controls.Add(this.rpMadre);
            this.Controls.Add(this.propietario);
            this.Controls.Add(this.rdg);
            this.Controls.Add(this.Sexo);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.rp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ingresoInfTer";
            this.Text = "INGRESO INFORMACIÓN DE ANIMAL";
            this.Load += new System.EventHandler(this.ingresoInfTer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label ubicacion;
        private System.Windows.Forms.Label noArete;
        private System.Windows.Forms.Label estado;
        private System.Windows.Forms.Label raza;
        private System.Windows.Forms.Label rpPadre;
        private System.Windows.Forms.Label rgdPadre;
        private System.Windows.Forms.Label rpMadre;
        private System.Windows.Forms.Label propietario;
        private System.Windows.Forms.Label rdg;
        private System.Windows.Forms.Label Sexo;
        private System.Windows.Forms.Label nombre;
        private System.Windows.Forms.Label rp;
    }
}