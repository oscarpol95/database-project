﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class animals : Form
    {
        public animals()
        {
            InitializeComponent();
        }

        private void animals_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet.Animals' Puede moverla o quitarla según sea necesario.
            /*this.animalsTableAdapter.Fill(this.database1DataSet.Animals);*/

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            main newform = new main();
            newform.Show();
            this.Dispose();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            //Ventana información general del animal
            if (radioButton5.Checked) {
                Form15 newform = new Form15();
                newform.Show();
                this.Close();
            }
            //Ventana Información de partos
            if (radioButton6.Checked) {
                Form16 newform = new Form16();
                newform.Show();
                this.Close();
            }
            //Ventana de información de planes reproductivos por vaca
            if (radioButton7.Checked) {
                Form19 newform = new Form19();
                newform.Show();
                this.Close();
            }
            //Ventana de información de pesajes por animal
            if (radioButton8.Checked) {
                Form17 newform = new Form17();
                newform.Show();
                this.Close();
            }
            //Ventana de información de planes sanitarios
            if (radioButton9.Checked) {
                Form18 newform = new Form18();
                newform.Show();
                this.Close();
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
           

        }

        private void animalsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.animalsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet);

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ingresoAnimalNuevo newform = new ingresoAnimalNuevo();
            newform.Show();
            this.Hide();
        }
    }
}
