﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form22 : Form
    {
        public Form22()
        {
            InitializeComponent();
        }

        private void áreasBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.áreasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form22_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Personal' Puede moverla o quitarla según sea necesario.
           // this.personalTableAdapter.Fill(this.database1DataSet1.Personal);
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Áreas' Puede moverla o quitarla según sea necesario.
           // this.áreasTableAdapter.Fill(this.database1DataSet1.Áreas);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ingresoDHacienda edit = new ingresoDHacienda();
            edit.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ingresoInfoArea newform = new ingresoInfoArea();
            newform.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ingresoInfoArea newform = new ingresoInfoArea();
            newform.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ingresoInfPersona newform = new ingresoInfPersona();
            newform.Show();
            this.Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ingresoInfPersona newform = new ingresoInfPersona();
            newform.Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            main newform = new main();
            newform.Show();
            this.Dispose();
        }
    }
}
