﻿namespace ProyectoBases
{
    partial class ingresoAnimalNuevo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ingresoAnimalNuevo));
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.descarte = new System.Windows.Forms.Button();
            this.guardar = new System.Windows.Forms.Button();
            this.sexoAnim = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.rgdAnim = new System.Windows.Forms.TextBox();
            this.nombreAnim = new System.Windows.Forms.TextBox();
            this.rpAnim = new System.Windows.Forms.TextBox();
            this.fechaLlegada = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fechaNacimiento = new System.Windows.Forms.Label();
            this.ubicacion = new System.Windows.Forms.Label();
            this.noArete = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.Label();
            this.raza = new System.Windows.Forms.Label();
            this.rpPadre = new System.Windows.Forms.Label();
            this.rgdPadre = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rpMadre = new System.Windows.Forms.Label();
            this.propietario = new System.Windows.Forms.Label();
            this.rdg = new System.Windows.Forms.Label();
            this.Sexo = new System.Windows.Forms.Label();
            this.nombre = new System.Windows.Forms.Label();
            this.rp = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Borrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(158, 251);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker2.TabIndex = 69;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(158, 221);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker1.TabIndex = 68;
            // 
            // descarte
            // 
            this.descarte.Location = new System.Drawing.Point(444, 319);
            this.descarte.Name = "descarte";
            this.descarte.Size = new System.Drawing.Size(92, 40);
            this.descarte.TabIndex = 67;
            this.descarte.Text = "Descartar";
            this.descarte.UseVisualStyleBackColor = true;
            this.descarte.Click += new System.EventHandler(this.descarte_Click);
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(179, 319);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(92, 40);
            this.guardar.TabIndex = 66;
            this.guardar.Text = "Guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // sexoAnim
            // 
            this.sexoAnim.FormattingEnabled = true;
            this.sexoAnim.Items.AddRange(new object[] {
            "MASCULINO",
            "FEMENINO"});
            this.sexoAnim.Location = new System.Drawing.Point(158, 76);
            this.sexoAnim.Name = "sexoAnim";
            this.sexoAnim.Size = new System.Drawing.Size(129, 21);
            this.sexoAnim.TabIndex = 65;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(430, 76);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(129, 21);
            this.comboBox3.TabIndex = 64;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(430, 23);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(129, 21);
            this.comboBox2.TabIndex = 63;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(158, 127);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(129, 21);
            this.comboBox1.TabIndex = 62;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(158, 277);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(131, 20);
            this.textBox13.TabIndex = 61;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(430, 101);
            this.textBox12.MaxLength = 30;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(131, 20);
            this.textBox12.TabIndex = 60;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(430, 49);
            this.textBox11.MaxLength = 5;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(131, 20);
            this.textBox11.TabIndex = 59;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(430, 131);
            this.textBox10.MaxLength = 30;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(131, 20);
            this.textBox10.TabIndex = 58;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(158, 179);
            this.textBox7.MaxLength = 5;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(131, 20);
            this.textBox7.TabIndex = 57;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(158, 153);
            this.textBox6.MaxLength = 5;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(131, 20);
            this.textBox6.TabIndex = 56;
            // 
            // rgdAnim
            // 
            this.rgdAnim.Location = new System.Drawing.Point(158, 101);
            this.rgdAnim.MaxLength = 5;
            this.rgdAnim.Name = "rgdAnim";
            this.rgdAnim.Size = new System.Drawing.Size(131, 20);
            this.rgdAnim.TabIndex = 55;
            // 
            // nombreAnim
            // 
            this.nombreAnim.Location = new System.Drawing.Point(158, 50);
            this.nombreAnim.MaxLength = 30;
            this.nombreAnim.Name = "nombreAnim";
            this.nombreAnim.Size = new System.Drawing.Size(131, 20);
            this.nombreAnim.TabIndex = 54;
            // 
            // rpAnim
            // 
            this.rpAnim.Location = new System.Drawing.Point(156, 23);
            this.rpAnim.MaxLength = 3;
            this.rpAnim.Name = "rpAnim";
            this.rpAnim.Size = new System.Drawing.Size(131, 20);
            this.rpAnim.TabIndex = 53;
            // 
            // fechaLlegada
            // 
            this.fechaLlegada.AutoSize = true;
            this.fechaLlegada.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.fechaLlegada.Location = new System.Drawing.Point(40, 252);
            this.fechaLlegada.Name = "fechaLlegada";
            this.fechaLlegada.Size = new System.Drawing.Size(96, 13);
            this.fechaLlegada.TabIndex = 52;
            this.fechaLlegada.Text = "Fecha de Llegada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.Location = new System.Drawing.Point(40, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "Hacienda Origen:";
            // 
            // fechaNacimiento
            // 
            this.fechaNacimiento.AutoSize = true;
            this.fechaNacimiento.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.fechaNacimiento.Location = new System.Drawing.Point(41, 221);
            this.fechaNacimiento.Name = "fechaNacimiento";
            this.fechaNacimiento.Size = new System.Drawing.Size(111, 13);
            this.fechaNacimiento.TabIndex = 50;
            this.fechaNacimiento.Text = "Fecha de Nacimiento:";
            // 
            // ubicacion
            // 
            this.ubicacion.AutoSize = true;
            this.ubicacion.Location = new System.Drawing.Point(358, 26);
            this.ubicacion.Name = "ubicacion";
            this.ubicacion.Size = new System.Drawing.Size(55, 13);
            this.ubicacion.TabIndex = 49;
            this.ubicacion.Text = "Ubicación";
            // 
            // noArete
            // 
            this.noArete.AutoSize = true;
            this.noArete.Location = new System.Drawing.Point(357, 56);
            this.noArete.Name = "noArete";
            this.noArete.Size = new System.Drawing.Size(52, 13);
            this.noArete.TabIndex = 48;
            this.noArete.Text = "No. Arete";
            // 
            // estado
            // 
            this.estado.AutoSize = true;
            this.estado.Location = new System.Drawing.Point(357, 84);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(43, 13);
            this.estado.TabIndex = 47;
            this.estado.Text = "Estado:";
            // 
            // raza
            // 
            this.raza.AutoSize = true;
            this.raza.Location = new System.Drawing.Point(357, 108);
            this.raza.Name = "raza";
            this.raza.Size = new System.Drawing.Size(35, 13);
            this.raza.TabIndex = 46;
            this.raza.Text = "Raza:";
            // 
            // rpPadre
            // 
            this.rpPadre.AutoSize = true;
            this.rpPadre.Location = new System.Drawing.Point(357, 134);
            this.rpPadre.Name = "rpPadre";
            this.rpPadre.Size = new System.Drawing.Size(56, 13);
            this.rpPadre.TabIndex = 45;
            this.rpPadre.Text = "RP Padre:";
            // 
            // rgdPadre
            // 
            this.rgdPadre.AutoSize = true;
            this.rgdPadre.Location = new System.Drawing.Point(357, 160);
            this.rgdPadre.Name = "rgdPadre";
            this.rgdPadre.Size = new System.Drawing.Size(65, 13);
            this.rgdPadre.TabIndex = 44;
            this.rgdPadre.Text = "RGD Padre:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "RP:";
            // 
            // rpMadre
            // 
            this.rpMadre.AutoSize = true;
            this.rpMadre.Location = new System.Drawing.Point(40, 160);
            this.rpMadre.Name = "rpMadre";
            this.rpMadre.Size = new System.Drawing.Size(58, 13);
            this.rpMadre.TabIndex = 42;
            this.rpMadre.Text = "RP Madre:";
            // 
            // propietario
            // 
            this.propietario.AutoSize = true;
            this.propietario.Location = new System.Drawing.Point(41, 134);
            this.propietario.Name = "propietario";
            this.propietario.Size = new System.Drawing.Size(57, 13);
            this.propietario.TabIndex = 41;
            this.propietario.Text = "Propietario";
            // 
            // rdg
            // 
            this.rdg.AutoSize = true;
            this.rdg.Location = new System.Drawing.Point(41, 108);
            this.rdg.Name = "rdg";
            this.rdg.Size = new System.Drawing.Size(34, 13);
            this.rdg.TabIndex = 40;
            this.rdg.Text = "RDG:";
            // 
            // Sexo
            // 
            this.Sexo.AutoSize = true;
            this.Sexo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Sexo.Location = new System.Drawing.Point(41, 84);
            this.Sexo.Name = "Sexo";
            this.Sexo.Size = new System.Drawing.Size(34, 13);
            this.Sexo.TabIndex = 39;
            this.Sexo.Text = "Sexo:";
            // 
            // nombre
            // 
            this.nombre.AutoSize = true;
            this.nombre.Location = new System.Drawing.Point(41, 52);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(47, 13);
            this.nombre.TabIndex = 38;
            this.nombre.Text = "Nombre:";
            // 
            // rp
            // 
            this.rp.AutoSize = true;
            this.rp.Location = new System.Drawing.Point(41, 26);
            this.rp.Name = "rp";
            this.rp.Size = new System.Drawing.Size(115, 13);
            this.rp.TabIndex = 37;
            this.rp.Text = "RP:                              ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(430, 160);
            this.textBox1.MaxLength = 30;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 20);
            this.textBox1.TabIndex = 70;
            // 
            // Borrar
            // 
            this.Borrar.Location = new System.Drawing.Point(308, 319);
            this.Borrar.Name = "Borrar";
            this.Borrar.Size = new System.Drawing.Size(92, 40);
            this.Borrar.TabIndex = 71;
            this.Borrar.Text = "Borrar";
            this.Borrar.UseVisualStyleBackColor = true;
            this.Borrar.Click += new System.EventHandler(this.Borrar_Click);
            // 
            // ingresoAnimalNuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 383);
            this.Controls.Add(this.Borrar);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.descarte);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.sexoAnim);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.rgdAnim);
            this.Controls.Add(this.nombreAnim);
            this.Controls.Add(this.rpAnim);
            this.Controls.Add(this.fechaLlegada);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fechaNacimiento);
            this.Controls.Add(this.ubicacion);
            this.Controls.Add(this.noArete);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.raza);
            this.Controls.Add(this.rpPadre);
            this.Controls.Add(this.rgdPadre);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rpMadre);
            this.Controls.Add(this.propietario);
            this.Controls.Add(this.rdg);
            this.Controls.Add(this.Sexo);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.rp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ingresoAnimalNuevo";
            this.Text = "INGRESO ANIMAL NUEVO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button descarte;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.ComboBox sexoAnim;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox rgdAnim;
        private System.Windows.Forms.TextBox nombreAnim;
        private System.Windows.Forms.TextBox rpAnim;
        private System.Windows.Forms.Label fechaLlegada;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label fechaNacimiento;
        private System.Windows.Forms.Label ubicacion;
        private System.Windows.Forms.Label noArete;
        private System.Windows.Forms.Label estado;
        private System.Windows.Forms.Label raza;
        private System.Windows.Forms.Label rpPadre;
        private System.Windows.Forms.Label rgdPadre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label rpMadre;
        private System.Windows.Forms.Label propietario;
        private System.Windows.Forms.Label rdg;
        private System.Windows.Forms.Label Sexo;
        private System.Windows.Forms.Label nombre;
        private System.Windows.Forms.Label rp;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Borrar;
    }
}