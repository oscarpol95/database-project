﻿namespace ProyectoBases
{
    partial class Form15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label rPLabel;
            System.Windows.Forms.Label nombreLabel;
            System.Windows.Forms.Label sexoLabel;
            System.Windows.Forms.Label rDGLabel;
            System.Windows.Forms.Label propietarioLabel;
            System.Windows.Forms.Label fecha_de_nacimientoLabel;
            System.Windows.Forms.Label fecha_de_llegadaLabel;
            System.Windows.Forms.Label hacienda_de_origenLabel;
            System.Windows.Forms.Label origenLabel;
            System.Windows.Forms.Label no__AreteLabel;
            System.Windows.Forms.Label estadoLabel;
            System.Windows.Forms.Label razaLabel;
            System.Windows.Forms.Label clasificaciónLabel;
            System.Windows.Forms.Label edadLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form15));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.rPTextBox = new System.Windows.Forms.TextBox();
            this.datosGeneralesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.database1DataSet1 = new ProyectoBases.Database1DataSet1();
            this.nombreTextBox = new System.Windows.Forms.TextBox();
            this.sexoTextBox = new System.Windows.Forms.TextBox();
            this.rDGTextBox = new System.Windows.Forms.TextBox();
            this.propietarioTextBox = new System.Windows.Forms.TextBox();
            this.fecha_de_nacimientoDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fecha_de_llegadaDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.hacienda_de_origenTextBox = new System.Windows.Forms.TextBox();
            this.origenTextBox = new System.Windows.Forms.TextBox();
            this.no__AreteTextBox = new System.Windows.Forms.TextBox();
            this.estadoTextBox = new System.Windows.Forms.TextBox();
            this.razaTextBox = new System.Windows.Forms.TextBox();
            this.clasificaciónTextBox = new System.Windows.Forms.TextBox();
            this.edadTextBox = new System.Windows.Forms.TextBox();
            this.datosGeneralesTableAdapter = new ProyectoBases.Database1DataSet1TableAdapters.DatosGeneralesTableAdapter();
            this.tableAdapterManager = new ProyectoBases.Database1DataSet1TableAdapters.TableAdapterManager();
            this.genealogiaTableAdapter = new ProyectoBases.Database1DataSet1TableAdapters.GenealogiaTableAdapter();
            this.datosGeneralesBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.datosGeneralesBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.genealogiaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genealogiaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            rPLabel = new System.Windows.Forms.Label();
            nombreLabel = new System.Windows.Forms.Label();
            sexoLabel = new System.Windows.Forms.Label();
            rDGLabel = new System.Windows.Forms.Label();
            propietarioLabel = new System.Windows.Forms.Label();
            fecha_de_nacimientoLabel = new System.Windows.Forms.Label();
            fecha_de_llegadaLabel = new System.Windows.Forms.Label();
            hacienda_de_origenLabel = new System.Windows.Forms.Label();
            origenLabel = new System.Windows.Forms.Label();
            no__AreteLabel = new System.Windows.Forms.Label();
            estadoLabel = new System.Windows.Forms.Label();
            razaLabel = new System.Windows.Forms.Label();
            clasificaciónLabel = new System.Windows.Forms.Label();
            edadLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosGeneralesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datosGeneralesBindingNavigator)).BeginInit();
            this.datosGeneralesBindingNavigator.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genealogiaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.genealogiaBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // rPLabel
            // 
            rPLabel.AutoSize = true;
            rPLabel.Location = new System.Drawing.Point(69, 48);
            rPLabel.Name = "rPLabel";
            rPLabel.Size = new System.Drawing.Size(25, 13);
            rPLabel.TabIndex = 0;
            rPLabel.Text = "RP:";
            // 
            // nombreLabel
            // 
            nombreLabel.AutoSize = true;
            nombreLabel.Location = new System.Drawing.Point(69, 74);
            nombreLabel.Name = "nombreLabel";
            nombreLabel.Size = new System.Drawing.Size(47, 13);
            nombreLabel.TabIndex = 2;
            nombreLabel.Text = "Nombre:";
            // 
            // sexoLabel
            // 
            sexoLabel.AutoSize = true;
            sexoLabel.Location = new System.Drawing.Point(69, 100);
            sexoLabel.Name = "sexoLabel";
            sexoLabel.Size = new System.Drawing.Size(34, 13);
            sexoLabel.TabIndex = 4;
            sexoLabel.Text = "Sexo:";
            // 
            // rDGLabel
            // 
            rDGLabel.AutoSize = true;
            rDGLabel.Location = new System.Drawing.Point(69, 126);
            rDGLabel.Name = "rDGLabel";
            rDGLabel.Size = new System.Drawing.Size(34, 13);
            rDGLabel.TabIndex = 6;
            rDGLabel.Text = "RDG:";
            // 
            // propietarioLabel
            // 
            propietarioLabel.AutoSize = true;
            propietarioLabel.Location = new System.Drawing.Point(69, 152);
            propietarioLabel.Name = "propietarioLabel";
            propietarioLabel.Size = new System.Drawing.Size(60, 13);
            propietarioLabel.TabIndex = 8;
            propietarioLabel.Text = "Propietario:";
            // 
            // fecha_de_nacimientoLabel
            // 
            fecha_de_nacimientoLabel.AutoSize = true;
            fecha_de_nacimientoLabel.Location = new System.Drawing.Point(69, 179);
            fecha_de_nacimientoLabel.Name = "fecha_de_nacimientoLabel";
            fecha_de_nacimientoLabel.Size = new System.Drawing.Size(109, 13);
            fecha_de_nacimientoLabel.TabIndex = 10;
            fecha_de_nacimientoLabel.Text = "Fecha de nacimiento:";
            // 
            // fecha_de_llegadaLabel
            // 
            fecha_de_llegadaLabel.AutoSize = true;
            fecha_de_llegadaLabel.Location = new System.Drawing.Point(69, 205);
            fecha_de_llegadaLabel.Name = "fecha_de_llegadaLabel";
            fecha_de_llegadaLabel.Size = new System.Drawing.Size(92, 13);
            fecha_de_llegadaLabel.TabIndex = 12;
            fecha_de_llegadaLabel.Text = "Fecha de llegada:";
            // 
            // hacienda_de_origenLabel
            // 
            hacienda_de_origenLabel.AutoSize = true;
            hacienda_de_origenLabel.Location = new System.Drawing.Point(69, 230);
            hacienda_de_origenLabel.Name = "hacienda_de_origenLabel";
            hacienda_de_origenLabel.Size = new System.Drawing.Size(103, 13);
            hacienda_de_origenLabel.TabIndex = 14;
            hacienda_de_origenLabel.Text = "Hacienda de origen:";
            // 
            // origenLabel
            // 
            origenLabel.AutoSize = true;
            origenLabel.Location = new System.Drawing.Point(69, 256);
            origenLabel.Name = "origenLabel";
            origenLabel.Size = new System.Drawing.Size(41, 13);
            origenLabel.TabIndex = 16;
            origenLabel.Text = "Origen:";
            // 
            // no__AreteLabel
            // 
            no__AreteLabel.AutoSize = true;
            no__AreteLabel.Location = new System.Drawing.Point(69, 282);
            no__AreteLabel.Name = "no__AreteLabel";
            no__AreteLabel.Size = new System.Drawing.Size(55, 13);
            no__AreteLabel.TabIndex = 18;
            no__AreteLabel.Text = "No  Arete:";
            // 
            // estadoLabel
            // 
            estadoLabel.AutoSize = true;
            estadoLabel.Location = new System.Drawing.Point(69, 308);
            estadoLabel.Name = "estadoLabel";
            estadoLabel.Size = new System.Drawing.Size(43, 13);
            estadoLabel.TabIndex = 20;
            estadoLabel.Text = "Estado:";
            // 
            // razaLabel
            // 
            razaLabel.AutoSize = true;
            razaLabel.Location = new System.Drawing.Point(69, 334);
            razaLabel.Name = "razaLabel";
            razaLabel.Size = new System.Drawing.Size(35, 13);
            razaLabel.TabIndex = 22;
            razaLabel.Text = "Raza:";
            // 
            // clasificaciónLabel
            // 
            clasificaciónLabel.AutoSize = true;
            clasificaciónLabel.Location = new System.Drawing.Point(69, 360);
            clasificaciónLabel.Name = "clasificaciónLabel";
            clasificaciónLabel.Size = new System.Drawing.Size(69, 13);
            clasificaciónLabel.TabIndex = 24;
            clasificaciónLabel.Text = "Clasificación:";
            // 
            // edadLabel
            // 
            edadLabel.AutoSize = true;
            edadLabel.Location = new System.Drawing.Point(69, 386);
            edadLabel.Name = "edadLabel";
            edadLabel.Size = new System.Drawing.Size(35, 13);
            edadLabel.TabIndex = 26;
            edadLabel.Text = "Edad:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(rPLabel);
            this.groupBox1.Controls.Add(this.rPTextBox);
            this.groupBox1.Controls.Add(nombreLabel);
            this.groupBox1.Controls.Add(this.nombreTextBox);
            this.groupBox1.Controls.Add(sexoLabel);
            this.groupBox1.Controls.Add(this.sexoTextBox);
            this.groupBox1.Controls.Add(rDGLabel);
            this.groupBox1.Controls.Add(this.rDGTextBox);
            this.groupBox1.Controls.Add(propietarioLabel);
            this.groupBox1.Controls.Add(this.propietarioTextBox);
            this.groupBox1.Controls.Add(fecha_de_nacimientoLabel);
            this.groupBox1.Controls.Add(this.fecha_de_nacimientoDateTimePicker);
            this.groupBox1.Controls.Add(fecha_de_llegadaLabel);
            this.groupBox1.Controls.Add(this.fecha_de_llegadaDateTimePicker);
            this.groupBox1.Controls.Add(hacienda_de_origenLabel);
            this.groupBox1.Controls.Add(this.hacienda_de_origenTextBox);
            this.groupBox1.Controls.Add(origenLabel);
            this.groupBox1.Controls.Add(this.origenTextBox);
            this.groupBox1.Controls.Add(no__AreteLabel);
            this.groupBox1.Controls.Add(this.no__AreteTextBox);
            this.groupBox1.Controls.Add(estadoLabel);
            this.groupBox1.Controls.Add(this.estadoTextBox);
            this.groupBox1.Controls.Add(razaLabel);
            this.groupBox1.Controls.Add(this.razaTextBox);
            this.groupBox1.Controls.Add(clasificaciónLabel);
            this.groupBox1.Controls.Add(this.clasificaciónTextBox);
            this.groupBox1.Controls.Add(edadLabel);
            this.groupBox1.Controls.Add(this.edadTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 472);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(251, 425);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Editar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // rPTextBox
            // 
            this.rPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "RP", true));
            this.rPTextBox.Location = new System.Drawing.Point(184, 45);
            this.rPTextBox.Name = "rPTextBox";
            this.rPTextBox.Size = new System.Drawing.Size(200, 20);
            this.rPTextBox.TabIndex = 1;
            // 
            // datosGeneralesBindingSource
            // 
            this.datosGeneralesBindingSource.DataMember = "DatosGenerales";
            this.datosGeneralesBindingSource.DataSource = this.database1DataSet1;
            // 
            // database1DataSet1
            // 
            this.database1DataSet1.DataSetName = "Database1DataSet1";
            this.database1DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nombreTextBox
            // 
            this.nombreTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Nombre", true));
            this.nombreTextBox.Location = new System.Drawing.Point(184, 71);
            this.nombreTextBox.Name = "nombreTextBox";
            this.nombreTextBox.Size = new System.Drawing.Size(200, 20);
            this.nombreTextBox.TabIndex = 3;
            // 
            // sexoTextBox
            // 
            this.sexoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Sexo", true));
            this.sexoTextBox.Location = new System.Drawing.Point(184, 97);
            this.sexoTextBox.Name = "sexoTextBox";
            this.sexoTextBox.Size = new System.Drawing.Size(200, 20);
            this.sexoTextBox.TabIndex = 5;
            // 
            // rDGTextBox
            // 
            this.rDGTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "RDG", true));
            this.rDGTextBox.Location = new System.Drawing.Point(184, 123);
            this.rDGTextBox.Name = "rDGTextBox";
            this.rDGTextBox.Size = new System.Drawing.Size(200, 20);
            this.rDGTextBox.TabIndex = 7;
            // 
            // propietarioTextBox
            // 
            this.propietarioTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Propietario", true));
            this.propietarioTextBox.Location = new System.Drawing.Point(184, 149);
            this.propietarioTextBox.Name = "propietarioTextBox";
            this.propietarioTextBox.Size = new System.Drawing.Size(200, 20);
            this.propietarioTextBox.TabIndex = 9;
            // 
            // fecha_de_nacimientoDateTimePicker
            // 
            this.fecha_de_nacimientoDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.datosGeneralesBindingSource, "Fecha de nacimiento", true));
            this.fecha_de_nacimientoDateTimePicker.Location = new System.Drawing.Point(184, 175);
            this.fecha_de_nacimientoDateTimePicker.Name = "fecha_de_nacimientoDateTimePicker";
            this.fecha_de_nacimientoDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.fecha_de_nacimientoDateTimePicker.TabIndex = 11;
            // 
            // fecha_de_llegadaDateTimePicker
            // 
            this.fecha_de_llegadaDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.datosGeneralesBindingSource, "Fecha de llegada", true));
            this.fecha_de_llegadaDateTimePicker.Location = new System.Drawing.Point(184, 201);
            this.fecha_de_llegadaDateTimePicker.Name = "fecha_de_llegadaDateTimePicker";
            this.fecha_de_llegadaDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.fecha_de_llegadaDateTimePicker.TabIndex = 13;
            // 
            // hacienda_de_origenTextBox
            // 
            this.hacienda_de_origenTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Hacienda de origen", true));
            this.hacienda_de_origenTextBox.Location = new System.Drawing.Point(184, 227);
            this.hacienda_de_origenTextBox.Name = "hacienda_de_origenTextBox";
            this.hacienda_de_origenTextBox.Size = new System.Drawing.Size(200, 20);
            this.hacienda_de_origenTextBox.TabIndex = 15;
            // 
            // origenTextBox
            // 
            this.origenTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Origen", true));
            this.origenTextBox.Location = new System.Drawing.Point(184, 253);
            this.origenTextBox.Name = "origenTextBox";
            this.origenTextBox.Size = new System.Drawing.Size(200, 20);
            this.origenTextBox.TabIndex = 17;
            // 
            // no__AreteTextBox
            // 
            this.no__AreteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "No_ Arete", true));
            this.no__AreteTextBox.Location = new System.Drawing.Point(184, 279);
            this.no__AreteTextBox.Name = "no__AreteTextBox";
            this.no__AreteTextBox.Size = new System.Drawing.Size(200, 20);
            this.no__AreteTextBox.TabIndex = 19;
            // 
            // estadoTextBox
            // 
            this.estadoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Estado", true));
            this.estadoTextBox.Location = new System.Drawing.Point(184, 305);
            this.estadoTextBox.Name = "estadoTextBox";
            this.estadoTextBox.Size = new System.Drawing.Size(200, 20);
            this.estadoTextBox.TabIndex = 21;
            // 
            // razaTextBox
            // 
            this.razaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Raza", true));
            this.razaTextBox.Location = new System.Drawing.Point(184, 331);
            this.razaTextBox.Name = "razaTextBox";
            this.razaTextBox.Size = new System.Drawing.Size(200, 20);
            this.razaTextBox.TabIndex = 23;
            // 
            // clasificaciónTextBox
            // 
            this.clasificaciónTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Clasificación", true));
            this.clasificaciónTextBox.Location = new System.Drawing.Point(184, 357);
            this.clasificaciónTextBox.Name = "clasificaciónTextBox";
            this.clasificaciónTextBox.Size = new System.Drawing.Size(200, 20);
            this.clasificaciónTextBox.TabIndex = 25;
            // 
            // edadTextBox
            // 
            this.edadTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.datosGeneralesBindingSource, "Edad", true));
            this.edadTextBox.Location = new System.Drawing.Point(184, 383);
            this.edadTextBox.Name = "edadTextBox";
            this.edadTextBox.Size = new System.Drawing.Size(200, 20);
            this.edadTextBox.TabIndex = 27;
            // 
            // datosGeneralesTableAdapter
            // 
            this.datosGeneralesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AnimalsTableAdapter = null;
            this.tableAdapterManager.ÁreasTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BajasTableAdapter = null;
            this.tableAdapterManager.DatosGeneralesTableAdapter = this.datosGeneralesTableAdapter;
            this.tableAdapterManager.GastosTableAdapter = null;
            this.tableAdapterManager.GenealogiaTableAdapter = this.genealogiaTableAdapter;
            this.tableAdapterManager.HistorialTableAdapter = null;
            this.tableAdapterManager.IngresosTableAdapter = null;
            this.tableAdapterManager.PartosTableAdapter = null;
            this.tableAdapterManager.PersonalTableAdapter = null;
            this.tableAdapterManager.PesajesTableAdapter = null;
            this.tableAdapterManager.PlanSanitarioTableAdapter = null;
            this.tableAdapterManager.TableTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ProyectoBases.Database1DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // genealogiaTableAdapter
            // 
            this.genealogiaTableAdapter.ClearBeforeFill = true;
            // 
            // datosGeneralesBindingNavigator
            // 
            this.datosGeneralesBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.datosGeneralesBindingNavigator.BindingSource = this.datosGeneralesBindingSource;
            this.datosGeneralesBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.datosGeneralesBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.datosGeneralesBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.datosGeneralesBindingNavigatorSaveItem});
            this.datosGeneralesBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.datosGeneralesBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.datosGeneralesBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.datosGeneralesBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.datosGeneralesBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.datosGeneralesBindingNavigator.Name = "datosGeneralesBindingNavigator";
            this.datosGeneralesBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.datosGeneralesBindingNavigator.Size = new System.Drawing.Size(873, 25);
            this.datosGeneralesBindingNavigator.TabIndex = 1;
            this.datosGeneralesBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Agregar nuevo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de elementos";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Eliminar";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primero";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posición";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posición actual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover siguiente";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // datosGeneralesBindingNavigatorSaveItem
            // 
            this.datosGeneralesBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.datosGeneralesBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("datosGeneralesBindingNavigatorSaveItem.Image")));
            this.datosGeneralesBindingNavigatorSaveItem.Name = "datosGeneralesBindingNavigatorSaveItem";
            this.datosGeneralesBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.datosGeneralesBindingNavigatorSaveItem.Text = "Guardar datos";
            this.datosGeneralesBindingNavigatorSaveItem.Click += new System.EventHandler(this.datosGeneralesBindingNavigatorSaveItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.genealogiaDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(454, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(374, 179);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Genealogía";
            // 
            // genealogiaDataGridView
            // 
            this.genealogiaDataGridView.AutoGenerateColumns = false;
            this.genealogiaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.genealogiaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.genealogiaDataGridView.DataSource = this.genealogiaBindingSource;
            this.genealogiaDataGridView.Location = new System.Drawing.Point(6, 38);
            this.genealogiaDataGridView.Name = "genealogiaDataGridView";
            this.genealogiaDataGridView.Size = new System.Drawing.Size(408, 114);
            this.genealogiaDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "RP";
            this.dataGridViewTextBoxColumn1.HeaderText = "RP";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Relación";
            this.dataGridViewTextBoxColumn2.HeaderText = "Relación";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Propietario";
            this.dataGridViewTextBoxColumn3.HeaderText = "Propietario";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Nombre del animal";
            this.dataGridViewTextBoxColumn4.HeaderText = "Nombre del animal";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "No_ Hijos";
            this.dataGridViewTextBoxColumn5.HeaderText = "No_ Hijos";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Estado";
            this.dataGridViewTextBoxColumn6.HeaderText = "Estado";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // genealogiaBindingSource
            // 
            this.genealogiaBindingSource.DataMember = "Genealogia";
            this.genealogiaBindingSource.DataSource = this.database1DataSet1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(460, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(368, 143);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Control de partos";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(224, 108);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(224, 71);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(224, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Meses abiertos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Días abiertos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha último Parto: ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(615, 456);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Volver";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form15
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 581);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.datosGeneralesBindingNavigator);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form15";
            this.Text = "Información de animal";
            this.Load += new System.EventHandler(this.Form15_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosGeneralesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datosGeneralesBindingNavigator)).EndInit();
            this.datosGeneralesBindingNavigator.ResumeLayout(false);
            this.datosGeneralesBindingNavigator.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.genealogiaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.genealogiaBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private Database1DataSet1 database1DataSet1;
        private System.Windows.Forms.BindingSource datosGeneralesBindingSource;
        private Database1DataSet1TableAdapters.DatosGeneralesTableAdapter datosGeneralesTableAdapter;
        private Database1DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator datosGeneralesBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton datosGeneralesBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox rPTextBox;
        private System.Windows.Forms.TextBox nombreTextBox;
        private System.Windows.Forms.TextBox sexoTextBox;
        private System.Windows.Forms.TextBox rDGTextBox;
        private System.Windows.Forms.TextBox propietarioTextBox;
        private System.Windows.Forms.DateTimePicker fecha_de_nacimientoDateTimePicker;
        private System.Windows.Forms.DateTimePicker fecha_de_llegadaDateTimePicker;
        private System.Windows.Forms.TextBox hacienda_de_origenTextBox;
        private System.Windows.Forms.TextBox origenTextBox;
        private System.Windows.Forms.TextBox no__AreteTextBox;
        private System.Windows.Forms.TextBox estadoTextBox;
        private System.Windows.Forms.TextBox razaTextBox;
        private System.Windows.Forms.TextBox clasificaciónTextBox;
        private System.Windows.Forms.TextBox edadTextBox;
        private Database1DataSet1TableAdapters.GenealogiaTableAdapter genealogiaTableAdapter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.BindingSource genealogiaBindingSource;
        private System.Windows.Forms.DataGridView genealogiaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}