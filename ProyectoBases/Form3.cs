﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class ingresoAnimalNuevo : Form
    {
        public ingresoAnimalNuevo()
        {
            InitializeComponent();
        }

        private void guardar_Click(object sender, EventArgs e)
        {
            animals newform = new animals();
            newform.Show();
            this.Dispose();
        }

        private void descarte_Click(object sender, EventArgs e)
        {

            animals newform = new animals();
            newform.Show();
            this.Dispose();
        }

        private void Borrar_Click(object sender, EventArgs e)
        {
            nombreAnim.Clear();
            rpAnim.Clear();
            textBox1.Clear();
            textBox10.Clear(); 
            textBox11.Clear();
            textBox12.Clear();
            rgdAnim.Clear();
            textBox13.Clear();
            textBox6.Clear();
            textBox7.Clear();
        }
    }
}
