﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form16 : Form
    {
        public Form16()
        {
            InitializeComponent();
        }

        private void partosBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.partosBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form16_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Partos' Puede moverla o quitarla según sea necesario.
            this.partosTableAdapter.Fill(this.database1DataSet1.Partos);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            animals newform = new animals();
            newform.Show();
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ingresoInfParto newform = new ingresoInfParto();
            newform.Show();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ingresoInfParto newform = new ingresoInfParto();
            newform.Show();
            this.Close();
        }
    }
}
