﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form17 : Form
    {
        public Form17()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pesajesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pesajesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form17_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Pesajes' Puede moverla o quitarla según sea necesario.
            this.pesajesTableAdapter.Fill(this.database1DataSet1.Pesajes);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            animals newform = new animals();
            newform.Show();
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ingresoInfoPesa newform = new ingresoInfoPesa();
            newform.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ingresoInfoPesa newform = new ingresoInfoPesa();
            newform.Show();
            this.Hide();
        }
    }
}
