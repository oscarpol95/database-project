﻿namespace ProyectoBases
{
    partial class ingresoMed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ingresoMed));
            this.descarte = new System.Windows.Forms.Button();
            this.guardar = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tipo = new System.Windows.Forms.Label();
            this.posologia = new System.Windows.Forms.Label();
            this.periodicidad = new System.Windows.Forms.Label();
            this.nombreMed = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // descarte
            // 
            this.descarte.Location = new System.Drawing.Point(311, 220);
            this.descarte.Name = "descarte";
            this.descarte.Size = new System.Drawing.Size(92, 40);
            this.descarte.TabIndex = 45;
            this.descarte.Text = "Descartar";
            this.descarte.UseVisualStyleBackColor = true;
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(134, 220);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(92, 40);
            this.guardar.TabIndex = 44;
            this.guardar.Text = "Guardar";
            this.guardar.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(147, 173);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 43;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(147, 123);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 42;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(147, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 41;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(381, 62);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 40;
            // 
            // tipo
            // 
            this.tipo.AutoSize = true;
            this.tipo.Location = new System.Drawing.Point(317, 70);
            this.tipo.Name = "tipo";
            this.tipo.Size = new System.Drawing.Size(31, 13);
            this.tipo.TabIndex = 39;
            this.tipo.Text = "Tipo:";
            // 
            // posologia
            // 
            this.posologia.AutoSize = true;
            this.posologia.Location = new System.Drawing.Point(68, 173);
            this.posologia.Name = "posologia";
            this.posologia.Size = new System.Drawing.Size(55, 13);
            this.posologia.TabIndex = 38;
            this.posologia.Text = "Posología";
            // 
            // periodicidad
            // 
            this.periodicidad.AutoSize = true;
            this.periodicidad.Location = new System.Drawing.Point(68, 123);
            this.periodicidad.Name = "periodicidad";
            this.periodicidad.Size = new System.Drawing.Size(68, 13);
            this.periodicidad.TabIndex = 37;
            this.periodicidad.Text = "Periodicidad:";
            // 
            // nombreMed
            // 
            this.nombreMed.AutoSize = true;
            this.nombreMed.Location = new System.Drawing.Point(68, 70);
            this.nombreMed.Name = "nombreMed";
            this.nombreMed.Size = new System.Drawing.Size(47, 13);
            this.nombreMed.TabIndex = 36;
            this.nombreMed.Text = "Nombre:";
            // 
            // ingresoMed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 323);
            this.Controls.Add(this.descarte);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.tipo);
            this.Controls.Add(this.posologia);
            this.Controls.Add(this.periodicidad);
            this.Controls.Add(this.nombreMed);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ingresoMed";
            this.Text = "INGRESO MEDICAMENTO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button descarte;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label tipo;
        private System.Windows.Forms.Label posologia;
        private System.Windows.Forms.Label periodicidad;
        private System.Windows.Forms.Label nombreMed;
    }
}