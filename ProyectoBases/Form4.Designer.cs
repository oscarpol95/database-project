﻿namespace ProyectoBases
{
    partial class ingresoInfoPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ingresoInfoPS));
            this.descartePM = new System.Windows.Forms.Button();
            this.guardarPM = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.rpAnimalPlan = new System.Windows.Forms.Label();
            this.medicamento = new System.Windows.Forms.Label();
            this.fechaDeAplicacion = new System.Windows.Forms.Label();
            this.dosis = new System.Windows.Forms.Label();
            this.tipoMed = new System.Windows.Forms.Label();
            this.nombrePlan = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // descartePM
            // 
            this.descartePM.Location = new System.Drawing.Point(315, 257);
            this.descartePM.Name = "descartePM";
            this.descartePM.Size = new System.Drawing.Size(75, 45);
            this.descartePM.TabIndex = 52;
            this.descartePM.Text = "Descartar";
            this.descartePM.UseVisualStyleBackColor = true;
            this.descartePM.Click += new System.EventHandler(this.descartePM_Click);
            // 
            // guardarPM
            // 
            this.guardarPM.Location = new System.Drawing.Point(165, 257);
            this.guardarPM.Name = "guardarPM";
            this.guardarPM.Size = new System.Drawing.Size(75, 45);
            this.guardarPM.TabIndex = 51;
            this.guardarPM.Text = "Guardar/ Cerrar";
            this.guardarPM.UseVisualStyleBackColor = true;
            this.guardarPM.Click += new System.EventHandler(this.guardarPM_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(119, 77);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 50;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(119, 122);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 49;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(119, 212);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 48;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(119, 167);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 47;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(360, 32);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 46;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(119, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 45;
            // 
            // rpAnimalPlan
            // 
            this.rpAnimalPlan.AutoSize = true;
            this.rpAnimalPlan.Location = new System.Drawing.Point(9, 85);
            this.rpAnimalPlan.Name = "rpAnimalPlan";
            this.rpAnimalPlan.Size = new System.Drawing.Size(59, 13);
            this.rpAnimalPlan.TabIndex = 44;
            this.rpAnimalPlan.Text = "RP Animal:";
            // 
            // medicamento
            // 
            this.medicamento.AutoSize = true;
            this.medicamento.Location = new System.Drawing.Point(9, 130);
            this.medicamento.Name = "medicamento";
            this.medicamento.Size = new System.Drawing.Size(74, 13);
            this.medicamento.TabIndex = 43;
            this.medicamento.Text = "Medicamento:";
            // 
            // fechaDeAplicacion
            // 
            this.fechaDeAplicacion.AutoSize = true;
            this.fechaDeAplicacion.Location = new System.Drawing.Point(9, 174);
            this.fechaDeAplicacion.Name = "fechaDeAplicacion";
            this.fechaDeAplicacion.Size = new System.Drawing.Size(104, 13);
            this.fechaDeAplicacion.TabIndex = 42;
            this.fechaDeAplicacion.Text = "Fecha de Aplicación";
            // 
            // dosis
            // 
            this.dosis.AutoSize = true;
            this.dosis.Location = new System.Drawing.Point(9, 219);
            this.dosis.Name = "dosis";
            this.dosis.Size = new System.Drawing.Size(101, 13);
            this.dosis.TabIndex = 41;
            this.dosis.Text = "Dosis Aplicada (mls)";
            // 
            // tipoMed
            // 
            this.tipoMed.AutoSize = true;
            this.tipoMed.Location = new System.Drawing.Point(280, 39);
            this.tipoMed.Name = "tipoMed";
            this.tipoMed.Size = new System.Drawing.Size(31, 13);
            this.tipoMed.TabIndex = 40;
            this.tipoMed.Text = "Tipo:";
            // 
            // nombrePlan
            // 
            this.nombrePlan.AutoSize = true;
            this.nombrePlan.Location = new System.Drawing.Point(9, 32);
            this.nombrePlan.Name = "nombrePlan";
            this.nombrePlan.Size = new System.Drawing.Size(47, 13);
            this.nombrePlan.TabIndex = 39;
            this.nombrePlan.Text = "Nombre:";
            // 
            // ingresoInfoPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 334);
            this.Controls.Add(this.descartePM);
            this.Controls.Add(this.guardarPM);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.rpAnimalPlan);
            this.Controls.Add(this.medicamento);
            this.Controls.Add(this.fechaDeAplicacion);
            this.Controls.Add(this.dosis);
            this.Controls.Add(this.tipoMed);
            this.Controls.Add(this.nombrePlan);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ingresoInfoPS";
            this.Text = "INGRESO INFORMACION PLAN SANITARIO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button descartePM;
        private System.Windows.Forms.Button guardarPM;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label rpAnimalPlan;
        private System.Windows.Forms.Label medicamento;
        private System.Windows.Forms.Label fechaDeAplicacion;
        private System.Windows.Forms.Label dosis;
        private System.Windows.Forms.Label tipoMed;
        private System.Windows.Forms.Label nombrePlan;
    }
}