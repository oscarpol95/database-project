﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form21 : Form
    {
        public Form21()
        {
            InitializeComponent();
        }

        private void ingresosBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.ingresosBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form21_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Gastos' Puede moverla o quitarla según sea necesario.
          //  this.gastosTableAdapter.Fill(this.database1DataSet1.Gastos);
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Ingresos' Puede moverla o quitarla según sea necesario.
           // this.ingresosTableAdapter.Fill(this.database1DataSet1.Ingresos);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ingresoInfoGastos newform = new ingresoInfoGastos();
            newform.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            main newform = new main();
            newform.Show();
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ingresoInfoGastos newform = new ingresoInfoGastos();
            newform.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form23 newform = new Form23();
            newform.Show();
            this.Hide();
        }
    }
}
