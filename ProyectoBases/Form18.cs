﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form18 : Form
    {
        public Form18()
        {
            InitializeComponent();
        }

        private void planSanitarioBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.planSanitarioBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form18_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.PlanSanitario' Puede moverla o quitarla según sea necesario.
            this.planSanitarioTableAdapter.Fill(this.database1DataSet1.PlanSanitario);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            animals newform = new animals();
            newform.Show();
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ingresoInfoPS newform = new ingresoInfoPS();
            newform.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ingresoInfoPS newform = new ingresoInfoPS();
            newform.Show();
            this.Hide();
        }
    }
}
