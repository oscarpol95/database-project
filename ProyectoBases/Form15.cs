﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBases
{
    public partial class Form15 : Form
    {
        public Form15()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void datosGeneralesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.datosGeneralesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet1);

        }

        private void Form15_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.Genealogia' Puede moverla o quitarla según sea necesario.
            this.genealogiaTableAdapter.Fill(this.database1DataSet1.Genealogia);
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet1.DatosGenerales' Puede moverla o quitarla según sea necesario.
            this.datosGeneralesTableAdapter.Fill(this.database1DataSet1.DatosGenerales);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            animals newform = new animals();
            newform.Show();
            this.Dispose();

        }
    }
}
