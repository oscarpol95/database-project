﻿namespace ProyectoBases
{
    partial class ingresoInfParto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ingresoInfParto));
            this.descarte = new System.Windows.Forms.Button();
            this.guardar = new System.Windows.Forms.Button();
            this.infoAnimal = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.estado = new System.Windows.Forms.Label();
            this.rpMadre = new System.Windows.Forms.Label();
            this.fechaPartoTernero = new System.Windows.Forms.Label();
            this.sexoTernero = new System.Windows.Forms.Label();
            this.rpPadreParto = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // descarte
            // 
            this.descarte.Location = new System.Drawing.Point(382, 251);
            this.descarte.Name = "descarte";
            this.descarte.Size = new System.Drawing.Size(81, 45);
            this.descarte.TabIndex = 50;
            this.descarte.Text = "DESCARTAR";
            this.descarte.UseVisualStyleBackColor = true;
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(281, 251);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(75, 45);
            this.guardar.TabIndex = 49;
            this.guardar.Text = "Guardar/ Cerrar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // infoAnimal
            // 
            this.infoAnimal.Location = new System.Drawing.Point(171, 251);
            this.infoAnimal.Name = "infoAnimal";
            this.infoAnimal.Size = new System.Drawing.Size(75, 45);
            this.infoAnimal.TabIndex = 48;
            this.infoAnimal.Text = "INFO ANIMAL";
            this.infoAnimal.UseVisualStyleBackColor = true;
            this.infoAnimal.Click += new System.EventHandler(this.infoAnimal_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "MASCULINO",
            "FEMENINO"});
            this.comboBox1.Location = new System.Drawing.Point(157, 123);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(129, 21);
            this.comboBox1.TabIndex = 47;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "MASCULINO",
            "FEMENINO"});
            this.comboBox4.Location = new System.Drawing.Point(416, 123);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(129, 21);
            this.comboBox4.TabIndex = 46;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(157, 61);
            this.textBox3.MaxLength = 3;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(131, 20);
            this.textBox3.TabIndex = 45;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(416, 61);
            this.textBox1.MaxLength = 3;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 20);
            this.textBox1.TabIndex = 43;
            // 
            // estado
            // 
            this.estado.AutoSize = true;
            this.estado.Location = new System.Drawing.Point(316, 131);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(43, 13);
            this.estado.TabIndex = 42;
            this.estado.Text = "Estado:";
            // 
            // rpMadre
            // 
            this.rpMadre.AutoSize = true;
            this.rpMadre.Location = new System.Drawing.Point(314, 68);
            this.rpMadre.Name = "rpMadre";
            this.rpMadre.Size = new System.Drawing.Size(58, 13);
            this.rpMadre.TabIndex = 41;
            this.rpMadre.Text = "RP Madre:";
            // 
            // fechaPartoTernero
            // 
            this.fechaPartoTernero.AutoSize = true;
            this.fechaPartoTernero.Location = new System.Drawing.Point(68, 202);
            this.fechaPartoTernero.Name = "fechaPartoTernero";
            this.fechaPartoTernero.Size = new System.Drawing.Size(83, 13);
            this.fechaPartoTernero.TabIndex = 40;
            this.fechaPartoTernero.Text = "Fecha de Parto:";
            // 
            // sexoTernero
            // 
            this.sexoTernero.AutoSize = true;
            this.sexoTernero.Location = new System.Drawing.Point(68, 131);
            this.sexoTernero.Name = "sexoTernero";
            this.sexoTernero.Size = new System.Drawing.Size(74, 13);
            this.sexoTernero.TabIndex = 39;
            this.sexoTernero.Text = "Sexo Ternero:";
            // 
            // rpPadreParto
            // 
            this.rpPadreParto.AutoSize = true;
            this.rpPadreParto.Location = new System.Drawing.Point(68, 68);
            this.rpPadreParto.Name = "rpPadreParto";
            this.rpPadreParto.Size = new System.Drawing.Size(56, 13);
            this.rpPadreParto.TabIndex = 38;
            this.rpPadreParto.Text = "RP Padre:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(156, 196);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker1.TabIndex = 51;
            // 
            // ingresoInfParto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 312);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.descarte);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.infoAnimal);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.rpMadre);
            this.Controls.Add(this.fechaPartoTernero);
            this.Controls.Add(this.sexoTernero);
            this.Controls.Add(this.rpPadreParto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ingresoInfParto";
            this.Text = "INGRESO INFORMACIÓN DE PARTO";
            this.Load += new System.EventHandler(this.ingresoInfParto_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button descarte;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.Button infoAnimal;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label estado;
        private System.Windows.Forms.Label rpMadre;
        private System.Windows.Forms.Label fechaPartoTernero;
        private System.Windows.Forms.Label sexoTernero;
        private System.Windows.Forms.Label rpPadreParto;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}